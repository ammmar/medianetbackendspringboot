package com.tn.medianet.backend.modele.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN,ROLE_DEPARTEMENT,ROLE_RH
}