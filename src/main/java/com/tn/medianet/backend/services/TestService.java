package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.TestRepository;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.Testquestion;
import com.tn.medianet.backend.services.Interfaces.TestInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TestService  implements TestInterface {

        @Autowired
       private  TestRepository testRepository;

    @Override
    public List<Test> getAllTests() {
        return this.testRepository.findAll();
    }

    @Override
    public Test Save(Test t) {
   return this.testRepository.save(t);
    }

    public  Test update(Test t)
    {
        return  this.testRepository.save(t);
    }

    public Optional<Test> getTestById(int id )
    {
        return  this.testRepository.findById(id);
    }


    public void delete(Test t){
        this.testRepository.delete(t);
    }







    public void afficheText(String msg)
    {
        System.out.println(msg);
    }



    public TestRepository getTestRepository() {
        return testRepository;
    }

    public void setTestRepository(TestRepository testRepository) {
        this.testRepository = testRepository;
    }
}
