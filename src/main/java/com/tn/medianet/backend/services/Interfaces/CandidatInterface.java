package com.tn.medianet.backend.services.Interfaces;

import com.tn.medianet.backend.model.Candidat;

import java.util.List;


public interface CandidatInterface {

    public List<Candidat> getAllCandiat();

}
