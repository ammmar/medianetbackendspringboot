package com.tn.medianet.backend.services;
import com.tn.medianet.backend.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.config.java.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;


@Component
public class SmtpMailSender {

    @Autowired
    private JavaMailSender javaMailSender;

    private static String Sender="ammar.hamza1995@gmail.com";
    public SmtpMailSender(JavaMailSender javaMailSender) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        /*mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(467);

        mailSender.setUsername("xyz@gmail.com");
        mailSender.setPassword("sadsadaffferere");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");*/
        this.javaMailSender = mailSender;

    }



    public void sendNotification(UserModel usermodel) throws MailException{

        StringBuilder sb = new StringBuilder();
        sb.append("Name: " + usermodel.getName()).append(System.lineSeparator());
        sb.append("\n Message: " + usermodel.getMessage());

        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(usermodel.getEmail());
        mail.setFrom(this.Sender);
        mail.setSubject(usermodel.getMessage());
        mail.setText(sb.toString());

        javaMailSender.send(mail);
    }

}
