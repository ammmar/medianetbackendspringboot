package com.tn.medianet.backend.services.Interfaces;

import com.tn.medianet.backend.model.Test;

import java.util.List;

public interface TestInterface {

   public  List<Test> getAllTests();
   public Test Save (Test t);

}
