package com.tn.medianet.backend.services.Interfaces;

import com.tn.medianet.backend.model.Question;

import java.util.List;

public interface QuestionInterface {

    List<Question> getAllQuestion();
}
