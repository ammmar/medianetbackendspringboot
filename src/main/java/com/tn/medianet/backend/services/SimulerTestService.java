package com.tn.medianet.backend.services;

import com.tn.medianet.backend.dao.TestQuestionRepository;
import com.tn.medianet.backend.dao.TestRepository;
import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.Testquestion;
import com.tn.medianet.backend.model.modelQuiz.Option;
import com.tn.medianet.backend.model.modelQuiz.QuestionQuiz;
import com.tn.medianet.backend.services.Interfaces.SimulerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulerTestService implements SimulerTest {

    @Autowired
    private TestQuestionRepository testQuestionRepository;

    @Autowired
    private TestRepository testRepository;



    @Override
    public List<Question> getAllTestInfoByIdTest(int id) {
        List<Question> listq = new ArrayList<Question>();
        List<Testquestion>listTQ=this.testQuestionRepository.findTestquestionByIdTest(id);
        Test test=new Test();

        for(int i=0;i<listTQ.size();i++)
        {
            Question q=new Question();
            q.setIdQuestion(listTQ.get(i).getQuestion().getIdQuestion());
            q.setDesgnQuestion(listTQ.get(i).getQuestion().getDesgnQuestion());
            q.setTempsQuestion(listTQ.get(i).getQuestion().getTempsQuestion());
            q.setUnite(listTQ.get(i).getQuestion().getUnite());
            q.setChoixs(listTQ.get(i).getQuestion().getChoixs());
            listq.add(q);
            // afficheText("listq "+listTQ.get(i).toString());
            afficheText("question "+i+" "+listq.get(i).toString());
        }

            return  listq;
}




    public List<QuestionQuiz> getAllTestInfoQuizByIdTest(int id) {
        List<Question> listq = new ArrayList<Question>();
        List<Testquestion>listTQ=this.testQuestionRepository.findTestquestionByIdTest(id);
        Test test=new Test();
        List<Option>optionList;
        Choix choix;
        Option option;
        QuestionQuiz questionQuiz;
        List<QuestionQuiz>questionQuizList=new ArrayList<QuestionQuiz>();
        for(int i=0;i<listTQ.size();i++) {
            optionList=new ArrayList<>();
            choix=new Choix();
            option=new Option();
            questionQuiz=new QuestionQuiz();
            List<Choix>choixList=listTQ.get(i).getQuestion().getChoixs();

            //remmplisaage des options
            for(int j=0;j<choixList.size();j++){
                choix=choixList.get(j);
                if(choix.getReponse()==1){
                    option=new Option(choix.getIdChoix(),listTQ.get(i).getQuestion().getIdQuestion(),
                            choix.getChoix(),true);
                }
                else if(choix.getReponse()==0){
                    option=new Option(choix.getIdChoix(),listTQ.get(i).getQuestion().getIdQuestion(),
                            choix.getChoix(),false);

                }
                option.setSelected(false);
                optionList.add(option);
                option=new Option();
            }
            questionQuiz.setOptions(optionList);
            questionQuiz.setId(listTQ.get(i).getQuestion().getIdQuestion());
            questionQuiz.setName(listTQ.get(i).getQuestion().getDesgnQuestion());
            questionQuiz.setQuestionTypeId(1);
            questionQuizList.add(questionQuiz);


        }

    return  questionQuizList;
    }






    public TestQuestionRepository getTestQuestionRepository() {
        return testQuestionRepository;
    }

    public void setTestQuestionRepository(TestQuestionRepository testQuestionRepository) {
        this.testQuestionRepository = testQuestionRepository;
    }

    public TestRepository getTestRepository() {
        return testRepository;
    }

    public void setTestRepository(TestRepository testRepository)
    {
        this.testRepository = testRepository;
    }
    public void afficheText(String msg)
    {
        System.out.println(msg);
    }

}
