package com.tn.medianet.backend.dao;

import com.tn.medianet.backend.model.Testquestion;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestQuestionRepository  extends JpaRepository<Testquestion,Integer>{
    @Query(
            value = "SELECT * FROM testquestion t WHERE t.id_test=:id",
            nativeQuery = true)
    List<Testquestion>findTestquestionByIdTest(@Param("id")int  id);

    @Query(value="call FINDDurationMinute(:test_id)",nativeQuery = true)
    int getTestDuration (@Param("test_id") Integer test_id);



}
