package com.tn.medianet.backend.dao;


import com.tn.medianet.backend.model.Test;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface TestRepository extends  JpaRepository<Test,Integer>{

    public List<Test> findAll();


}
