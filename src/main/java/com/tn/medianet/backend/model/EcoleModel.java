package com.tn.medianet.backend.model;

import org.springframework.beans.factory.annotation.Value;

public class EcoleModel {
   private String ecole;
   private int count;

    public EcoleModel() {

    }

    public EcoleModel(String ecole, int count) {
        this.ecole = ecole;
        this.count = count;
    }

    public String getEcole() {
        return ecole;
    }

    public void setEcole(String ecole) {
        this.ecole = ecole;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
