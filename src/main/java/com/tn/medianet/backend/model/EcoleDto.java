package com.tn.medianet.backend.model;

import javax.persistence.criteria.CriteriaBuilder;

public interface EcoleDto {

    String getEcole();
    Integer getCount();
}
