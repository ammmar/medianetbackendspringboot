package com.tn.medianet.backend.model;

public class Statistique {

    private int nombreQuestion;
    private int nombreTest;
    private int nombreCandiat;

    public Statistique() {
    }

    @Override
    public String toString() {
        return "Statistique{" +
                "nombreQuestion=" + nombreQuestion +
                ", nombreTest=" + nombreTest +
                ", nombreCandiat=" + nombreCandiat +
                '}';
    }

    public int getNombreQuestion() {
        return nombreQuestion;
    }

    public void setNombreQuestion(int nombreQuestion) {
        this.nombreQuestion = nombreQuestion;
    }

    public int getNombreTest() {
        return nombreTest;
    }

    public void setNombreTest(int nombreTest) {
        this.nombreTest = nombreTest;
    }

    public int getNombreCandiat() {
        return nombreCandiat;
    }

    public void setNombreCandiat(int nombreCandiat) {
        this.nombreCandiat = nombreCandiat;
    }
}
