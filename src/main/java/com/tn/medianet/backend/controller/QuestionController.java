package com.tn.medianet.backend.controller;

import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.Testquestion;
import com.tn.medianet.backend.services.QuestionService;
import com.tn.medianet.backend.services.TestQuestionService;
import com.tn.medianet.backend.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.util.*;

@RestController
@RequestMapping(value ="/auth")
@CrossOrigin(origins = "*")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private TestService testService;

    @Autowired
    private TestQuestionService testQuestionService;


    @RequestMapping(value = "/question/getallQuestion", method = RequestMethod.GET)
    public ResponseEntity getallQuestion(){
        List<Question>listq=new ArrayList<Question>();

        listq=this.questionService.getAllQuestion();
        if(listq.size()>0)
        {
            return new ResponseEntity<>(listq,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/question/deletequestion/{id}")
    public ResponseEntity DeleteQuestion(@PathVariable("id")int id_question){
       Question question=this.questionService.getQuestionById(id_question);
        if(question!=null)
        {
            this.questionService.delete(question);
            return new ResponseEntity<>(question,HttpStatus.OK);

        }
        else{
            this.afficheText("question not found"+id_question);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }



    //** Save Or Update
    @PostMapping(value="/question/creerUpdateQuestion/")
    public ResponseEntity SaveOrUpdateQuestion(@RequestBody Question question) {
        afficheText("QuestionQuiz:id question"+question.getIdQuestion());

    Question q=this.questionService.save(question);
    if(q!=null){
        return new ResponseEntity<>(q,HttpStatus.OK);

    }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }


    //************ Creer Test
    @PutMapping(value="/question/creerQuestion/{id}")
    public ResponseEntity AjouterQuestionTest(@RequestBody Question question, @PathVariable("id") int id_test) {

        Random objGenerator = new Random();
        int randomNumber =30+ objGenerator.nextInt(10000);
        System.out.println("Random No : " + randomNumber);
        question.setIdQuestion(randomNumber);
        Question questionSaved=new Question();
       questionSaved=this.questionService.save(question);

        Test t=new Test();
        Testquestion testquestion=new Testquestion();

        Optional<Test> to=this.testService.getTestById(id_test);
        if(to.isPresent()){
            t=to.get();
        }
        afficheText("saved question  "+questionSaved.toString());

        if(t!=null) {
            testquestion.setQuestion(questionSaved);
            testquestion.setTest(t);
            afficheText("TestQuestion"+testquestion.toString());
            Testquestion TQsaved=this.testQuestionService.ajouter(testquestion);
            afficheText("QuestionController:CreerTest:TestQuestionSaved"+TQsaved.toString());
        }else{
        afficheText("probleme d'ajouter TestQuestion");
        }


            if(questionSaved!=null) {

                return new ResponseEntity<>(question,HttpStatus.OK);
            }
            else{
                return new  ResponseEntity(HttpStatus.NOT_FOUND);
            }

    }





    //**********Get QuestionQuiz By Id
    @GetMapping(value="/question/getQuestiontById/{id}")
    public ResponseEntity getTestById(@PathVariable("id") int id) {
        Question q=this.questionService.getQuestionById(id);
        if(q!=null)
        return new ResponseEntity<>(q, HttpStatus.OK);
        else
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }



    //**************** partie choix

    //****** get choix by question id

    @GetMapping(value="/question/getChoixsByQuestionId/{id}")
    public ResponseEntity getchoixsByQuestionId(@PathVariable("id") int id_question){
        List<Choix>listchoix=new ArrayList<Choix>();
        Question q=this.questionService.getQuestionById(id_question);
        if(q!=null){
            listchoix=q.getChoixs();
            if(listchoix.size()>0){
                return new ResponseEntity<>( listchoix,HttpStatus.OK);

            }else {
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }
        }else{
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);

        }
    }


    public void afficheText(String msg)
    {
        System.out.println(msg);
    }



    public TestService getTestService() {
        return testService;
    }

    public void setTestService(TestService testService) {
        this.testService = testService;
    }

    public QuestionService getQuestionService() {
        return questionService;
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    public TestQuestionService getTestQuestionService() {
        return testQuestionService;
    }

    public void setTestQuestionService(TestQuestionService testQuestionService) {
        this.testQuestionService = testQuestionService;
    }
}
