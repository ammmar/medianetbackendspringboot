package com.tn.medianet.backend.controller;

import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.services.CandidatService;
import com.tn.medianet.backend.services.TestQuestionService;
import com.tn.medianet.backend.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.io.ByteArrayOutputStream;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;




@RestController
@RequestMapping(value ="/auth")
@CrossOrigin(origins = "*")
public class TestController {



    @Autowired
    private CandidatService candidatService;

    @Autowired
    private TestService testService;


    @Autowired
    private TestQuestionService testQuestionService;

    //************* All Test***********
    @RequestMapping(value = "/test/getAllTest", method = RequestMethod.GET)
    public ResponseEntity  getAllTest()
    {
        List<Test> listt=testService.getAllTests();

            if(listt!=null)
            {

                if(listt.size()==0){
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);

                }
                else{
                    return new ResponseEntity<>(listt, HttpStatus.OK);

                }
            }
            else
            {
                return new ResponseEntity<>(listt, HttpStatus.OK);

            }
    }



    //************* All Test***********
    @RequestMapping(value = "/test/getAllTest/etatTest", method = RequestMethod.GET)
    public ResponseEntity  getAllTestEtat()
    {

        List<Test> listt=testService.getAllTests();

        List<Test>ListEtat=new ArrayList<Test>();
        if(listt!=null) {
            for (int i = 0; i < listt.size(); i++) {
                    Test t=listt.get(i);
                    t.setNomcandidat(t.getCandidat().getNom());
                    t.setPrenomcandidat(t.getCandidat().getPrenom());
                    t.setId_candidat(t.getCandidat().getIdCandidat());
                    t.setNumTel(t.getCandidat().getTel());
                    if(t.getEtatTest()==1){
                        t.setEtatTestInfo("Passé");
                    }
                    else{
                        t.setEtatTestInfo("En Cours");
                    }
                    if(t.getCandidat().getIdCandidat()!=2)
                    ListEtat.add(t);
            }
        }

        if(ListEtat.size()==0){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
        else{
            return new ResponseEntity<>(ListEtat, HttpStatus.OK);

        }


    }

    //************ Creer Test
    @PutMapping(value="/test/creerTest/{id}")
    public ResponseEntity AjouterTest(@RequestBody Test test,@PathVariable("id")int  id_candidat)  {
       // test.setIdTest(0);
       /* System.out.println("Original Image Byte Size - " + file.getBytes().length);
            test.setLogo(compressBytes(file.getBytes()));*/
         //System.out.print("Test info "+test.toString());

        if(id_candidat>0) {
             System.out.print("id candiat valide");
             test.setCandidat(this.candidatService.getCandiatById(id_candidat));
         }
         else{
             System.out.print("id candiat vide ");
         }

        Test t=this.testService.Save(test);
        System.out.print("  test  info "+test.toString()+"canadiat"+test.getCandidat().toString());
        //Test t=null;
        if(t!=null)
        return new ResponseEntity<>(t, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);


    }


//****************afecter test
    @PutMapping(value="/test/affecterTest/{id_test}/{id_candidat}")
    public ResponseEntity AffecterTest(@PathVariable("id_test")int  id_test,@PathVariable("id_candidat")int  id_candidat,@RequestBody Test testa)  {

        Test test=new Test();
        this.afficheText("***************test info date debut  "+testa.getDateDebut()+"date fin"+testa.getDateFin());
        if(id_test>0 && id_candidat>0) {
            System.out.print("id candiat && test no null");
            Optional<Test>optest=this.testService.getTestById(id_test);

            if(optest.isPresent()) {
                test=optest.get();

                /*
                	this.dateDebut = dateDebut;
		this.dateEvaluation = dateEvaluation;
		this.dateFin = dateFin;
		this.description = description;
		this.etatTest = etatTest;
		this.score = score;
		this.theme = theme;
		this.typeTest = typeTest;
		this.numTel = numTel;
                 */

                if(test.getCandidat().getIdCandidat()!=id_candidat && id_candidat!=2) {

                    test.setCandidat(this.candidatService.getCandiatById(id_candidat));

                   test.setDateDebut(testa.getDateDebut());
                    test.setDateFin(testa.getDateFin());
                    test.setScore(0);
                    test.setEtatTest(0);

                    Test testp=new Test(testa.getDateDebut(),testa.getDateFin(),test.getDescription(),test.getEtatTest()
                    ,test.getScore(),test.getTheme(),test.getTypeTest(),test.getNumTel());
                    testp.setCandidat(this.candidatService.getCandiatById(id_candidat));
                    testp.setTestquestions(test.getTestquestions());
                    testp.setIdTest(100);
                   // this.afficheText("test is ==>"+test.toString()+"candiat test"+test.getCandidat().toString());
                    //this.afficheText("candiat"+this.candidatService.getCandiatById(id_candidat).toString());
                    this.testService.Save(test);
                    return new ResponseEntity<>(test, HttpStatus.OK);
                }else if(test.getCandidat().getIdCandidat()==2 ){

                    test.setCandidat(this.candidatService.getCandiatById(id_candidat));
                    test.setDateDebut(testa.getDateDebut());
                    test.setDateFin(testa.getDateFin());
                    test.setScore(0);
                    test.setEtatTest(0);
                    this.testService.Save(test);
                    return new ResponseEntity<>(test, HttpStatus.OK);
                }

            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }


        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }



    //************ get Test By id
    @GetMapping(value="/test/getTestById/{id}")
    public ResponseEntity getTestById(@PathVariable("id") int id) {
        Test t=null;
        Optional<Test>to=this.testService.getTestById(id);
        System.out.println("Test Controller test duration "+this.testQuestionService.getTestDuration(id)*60);

        if(to.isPresent()) {
            t = to.get();

            t.setDurationTest(this.testQuestionService.getTestDuration(id)*60);
            System.out.print("id test "+t.toString());
        }
        if(t!=null)
            return new ResponseEntity<>(t, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    //************* modifier Test
    @PutMapping(value="/test/modifierTest/{id}")
    public ResponseEntity upadteTest(@RequestBody Test test,@PathVariable("id")int  id_candidat)  {

        if(id_candidat>0) {
            System.out.print("id candiat valide");
            test.setCandidat(this.candidatService.getCandiatById(id_candidat));
            System.out.print(" modifierTestCandiat: test  info "+test.toString());

        }
        else{
            System.out.print("id candiat vide ");
        }



        Test t=this.testService.Save(test);
        System.out.print(" modifierTest: test  info "+test.toString());
        //Test t=null;
        if(t!=null)
            return new ResponseEntity<>(t, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);


    }


    //************ get Questions By Id Test

    @GetMapping(value="/test/getQuestionByIdTest/{id}")
    public ResponseEntity getQuestionTestById(@PathVariable("id") int id) {
        List<Question>listq=new ArrayList<Question>();
        listq=this.testQuestionService.getAllQuestionByIdTest(id);

        if(listq.size()>0   )
            return new ResponseEntity<>(listq, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @DeleteMapping(value="/test/deleteTest/{id}")
    public ResponseEntity AffecterTest(@PathVariable("id")int  id_test) {
        Test t=null;
        Optional<Test>to=this.testService.getTestById(id_test);
        if(to.isPresent()) {
            this.testService.delete(to.get());
            t=to.get();
            System.out.print("id test "+to.get());
        }
        if(t!=null)
            return new ResponseEntity<>(t, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }





    public void afficheText(String msg)
    {
        System.out.println(msg);
    }





    public int getTimeOnSecond(Test test){

        return 0;
    }





    // compress the image bytes before storing it in the database
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }
    // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
        } catch (DataFormatException e) {
        }
        return outputStream.toByteArray();
    }

}










