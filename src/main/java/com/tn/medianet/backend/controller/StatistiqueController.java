package com.tn.medianet.backend.controller;

import com.tn.medianet.backend.model.EcoleDto;
import com.tn.medianet.backend.model.EcoleModel;
import com.tn.medianet.backend.model.Statistique;
import com.tn.medianet.backend.services.CandidatService;
import com.tn.medianet.backend.services.QuestionService;
import com.tn.medianet.backend.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value ="/auth")
@CrossOrigin(origins = "*")
public class StatistiqueController {
    @Autowired
    private CandidatService candidatService;

    @Autowired
    private TestService testService;


    @Autowired
    private QuestionService questionService;

    //************* All Test***********
    @RequestMapping(value = "/stat/keyinfo", method = RequestMethod.GET)
    public ResponseEntity getKeyInfo() {
        Statistique statistique = new Statistique();
        int nbtest = 0;
        int nbquestion = 0;
        int nbcandiat = 0;
        if (this.candidatService.getAllCandiat() != null && this.testService.getAllTests() != null && this.questionService.getAllQuestion() != null) {

            nbcandiat = this.candidatService.getAllCandiat().size();
            nbquestion = this.questionService.getAllQuestion().size();
            nbtest = this.candidatService.getAllCandiat().size();
        }
        statistique.setNombreCandiat(nbcandiat);
        statistique.setNombreTest(nbtest);
        statistique.setNombreQuestion(nbquestion);
        return new ResponseEntity<>(statistique, HttpStatus.OK);
    }

    @RequestMapping(value = "/stat/ecolestat", method = RequestMethod.GET)
    public ResponseEntity getEcoleStat() {
        List<EcoleModel> liste=new ArrayList<EcoleModel>();
        liste = this.candidatService.getAllStatEcoleCount();

if(liste.size()>0) {
    return new ResponseEntity<>(liste, HttpStatus.OK);
}
else{
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
}

    }


}