-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 27 mai 2020 à 16:04
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `medianetbackend`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `FINDDurationMinute`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `FINDDurationMinute` (IN `idtest` INT(100) UNSIGNED)  SELECT sum(q.temps_question) from testquestion  NATURAL join question q          WHERE testquestion.id_test=idtest$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `authority`
--

DROP TABLE IF EXISTS `authority`;
CREATE TABLE IF NOT EXISTS `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `authority`
--

INSERT INTO `authority` (`id`, `name`) VALUES
(4, 'ROLE_ADMIN'),
(5, 'ROLE_ADMIN'),
(6, 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Structure de la table `authority_seq`
--

DROP TABLE IF EXISTS `authority_seq`;
CREATE TABLE IF NOT EXISTS `authority_seq` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `authority_seq`
--

INSERT INTO `authority_seq` (`next_val`) VALUES
(7);

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

DROP TABLE IF EXISTS `candidat`;
CREATE TABLE IF NOT EXISTS `candidat` (
  `id_candidat` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `ecole` varchar(200) NOT NULL,
  `experience` varchar(200) NOT NULL,
  `Niveau` int(11) NOT NULL,
  `profil` varchar(20) NOT NULL,
  `date_debot` date NOT NULL DEFAULT current_timestamp(),
  `cv` blob DEFAULT NULL,
  `date_Evaluation` date DEFAULT NULL,
  PRIMARY KEY (`id_candidat`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `candidat`
--

INSERT INTO `candidat` (`id_candidat`, `nom`, `prenom`, `email`, `password`, `tel`, `ecole`, `experience`, `Niveau`, `profil`, `date_debot`, `cv`, `date_Evaluation`) VALUES
(2, 'hamza', 'ammar', 'ammar.hamza1995@gmail.com', 'h123', '53652673', 'isaam', '2', 3, 'front End', '2020-05-20', NULL, NULL),
(3, 'sihem', 'bouzid', 'mourad123@yahoo.fr', 'h123', '25698741', 'ENSI', '1', 5, 'Backend ', '2020-05-20', NULL, NULL),
(4, 'bargawi', 'asma', 'ammar.asma1995@gmail.com', 'h123', '25698747', 'isaam', '2', 5, 'front End', '2020-04-06', NULL, NULL),
(11, 'abla ', 'sghaier', 'abla@gmail.com', '123', '25247898', 'ENSI', '0', 5, 'base de donnes', '2020-04-27', NULL, NULL),
(12, 'khawla', 'sliti', 'khawla123@yahoo.fr', '123', '58741859', 'iset', '1', 5, 'backned,Front', '2020-05-08', NULL, NULL),
(17, 'mohamed', 'ben hossin', 'mohamed@yahoo.fr', 'h123', '50959590', 'iset', '3', 4, 'design', '2020-05-27', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `candidat_reponse_choix`
--

DROP TABLE IF EXISTS `candidat_reponse_choix`;
CREATE TABLE IF NOT EXISTS `candidat_reponse_choix` (
  `id_crc` int(11) NOT NULL AUTO_INCREMENT,
  `id_reponse` int(11) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  `id_choix` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_crc`),
  KEY `id_reponse` (`id_reponse`),
  KEY `id_candidat` (`id_candidat`),
  KEY `id_choix` (`id_choix`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `choix`
--

DROP TABLE IF EXISTS `choix`;
CREATE TABLE IF NOT EXISTS `choix` (
  `id_choix` int(11) NOT NULL AUTO_INCREMENT,
  `choix` text NOT NULL,
  `reponse` int(1) NOT NULL,
  `id_question` int(11) NOT NULL,
  PRIMARY KEY (`id_choix`),
  KEY `id_question` (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `choix`
--

INSERT INTO `choix` (`id_choix`, `choix`, `reponse`, `id_question`) VALUES
(6, '<p>', 0, 4),
(7, '<h1>', 0, 4),
(8, '<h2>', 1, 4),
(20, '<footer>', 0, 3),
(21, 'angular', 0, 5),
(22, 'react', 0, 5),
(23, 'vue', 1, 5),
(24, '<title>', 0, 3),
(25, '<html>', 0, 3),
(26, '<p>', 0, 3),
(27, 'join', 1, 511),
(28, 'where', 0, 511),
(29, 'having', 0, 511),
(30, 'relationnel', 1, 144),
(31, 'no relationnel', 0, 144),
(32, 'fichier', 0, 144),
(33, '<h5>', 1, 3),
(34, 'dépassement memoire ', 0, 4281),
(35, 'variable inconnu', 1, 4281),
(36, 'bibliothèque ', 0, 4281),
(37, 'langage', 0, 7021),
(38, 'satandard', 1, 7021),
(39, 'varaible', 0, 7021),
(40, '<thead>', 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(190),
(190),
(190),
(190),
(190),
(190),
(190),
(190),
(190),
(190),
(190),
(190);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `desgn_question` text NOT NULL,
  `temps_question` int(11) NOT NULL,
  `unite` varchar(100) NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=9373 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id_question`, `desgn_question`, `temps_question`, `unite`) VALUES
(3, 'En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?', 1, 'minute'),
(4, 'Lequel des élements suivants ne sont plus pris en charge dans HTML5', 2, 'minute'),
(5, 'parmi les proposition suivantes lequels une Framework front end ?!', 2, 'minute'),
(6, 'react est un ?', 1, 'minute'),
(7, 'l\'élement <canvas> en  HTML5 est utilisé pour: ?', 3, 'minute '),
(144, 'MYSQL EST UN BASE DE DONNEES', 2, 'minute'),
(511, 'quelle est le type de jointure', 2, 'minute'),
(4281, 'que signifie l\'erreur segmentation fault', 2, 'minute'),
(7021, 'ES6', 2, 'minute');

-- --------------------------------------------------------

--
-- Structure de la table `reponse_candidat`
--

DROP TABLE IF EXISTS `reponse_candidat`;
CREATE TABLE IF NOT EXISTS `reponse_candidat` (
  `id_reponse` int(11) NOT NULL AUTO_INCREMENT,
  `reponse` int(1) NOT NULL,
  PRIMARY KEY (`id_reponse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id_test` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `logo` blob DEFAULT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `type_test` varchar(100) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  `score` float NOT NULL,
  `etat_test` int(11) NOT NULL,
  `date_evaluation` date DEFAULT NULL,
  `resultat` text NOT NULL,
  PRIMARY KEY (`id_test`),
  KEY `id_candidat` (`id_candidat`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `test`
--

INSERT INTO `test` (`id_test`, `theme`, `description`, `logo`, `date_debut`, `date_fin`, `type_test`, `id_candidat`, `score`, `etat_test`, `date_evaluation`, `resultat`) VALUES
(2, 'html5', 'HTML5', NULL, '2020-05-27', '2020-05-28', 'Technique', 12, 33.3333, 1, '2020-05-27', '[\n  {\n    \"id\": 3,\n    \"name\": \"En Html 5 cet élément est utilisé pour grouper des éléments rubrique ?\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 20,\n        \"questionId\": 3,\n        \"name\": \"\\u003cfooter\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 24,\n        \"questionId\": 3,\n        \"name\": \"\\u003ctitle\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 25,\n        \"questionId\": 3,\n        \"name\": \"\\u003chtml\\u003e\",\n        \"answer\": false,\n        \"selected\": true\n      },\n      {\n        \"id\": 26,\n        \"questionId\": 3,\n        \"name\": \"\\u003cp\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 33,\n        \"questionId\": 3,\n        \"name\": \"\\u003ch5\\u003e\",\n        \"answer\": true,\n        \"selected\": false\n      },\n      {\n        \"id\": 40,\n        \"questionId\": 3,\n        \"name\": \"\\u003cthead\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      }\n    ]\n  },\n  {\n    \"id\": 4,\n    \"name\": \"Lequel des élements suivants ne sont plus pris en charge dans HTML5\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 6,\n        \"questionId\": 4,\n        \"name\": \"\\u003cp\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 7,\n        \"questionId\": 4,\n        \"name\": \"\\u003ch1\\u003e\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 8,\n        \"questionId\": 4,\n        \"name\": \"\\u003ch2\\u003e\",\n        \"answer\": true,\n        \"selected\": true\n      }\n    ]\n  },\n  {\n    \"id\": 5,\n    \"name\": \"parmi les proposition suivantes lequels une Framework front end ?!\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 21,\n        \"questionId\": 5,\n        \"name\": \"angular\",\n        \"answer\": false,\n        \"selected\": true\n      },\n      {\n        \"id\": 22,\n        \"questionId\": 5,\n        \"name\": \"react\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 23,\n        \"questionId\": 5,\n        \"name\": \"vue\",\n        \"answer\": true,\n        \"selected\": false\n      }\n    ]\n  }\n]'),
(7, 'base de donness', 'relationnel', NULL, '2020-05-07', '2020-05-25', 'logique', 3, 50, 1, NULL, '[\n  {\n    \"id\": 511,\n    \"name\": \"quelle est le type de jointure\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 27,\n        \"questionId\": 511,\n        \"name\": \"join\",\n        \"answer\": true,\n        \"selected\": false\n      },\n      {\n        \"id\": 28,\n        \"questionId\": 511,\n        \"name\": \"where\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 29,\n        \"questionId\": 511,\n        \"name\": \"having\",\n        \"answer\": false,\n        \"selected\": true\n      }\n    ]\n  },\n  {\n    \"id\": 144,\n    \"name\": \"MYSQL EST UN BASE DE DONNEES\",\n    \"questionTypeId\": 1,\n    \"options\": [\n      {\n        \"id\": 30,\n        \"questionId\": 144,\n        \"name\": \"relationnel\",\n        \"answer\": true,\n        \"selected\": true\n      },\n      {\n        \"id\": 31,\n        \"questionId\": 144,\n        \"name\": \"no relationnel\",\n        \"answer\": false,\n        \"selected\": false\n      },\n      {\n        \"id\": 32,\n        \"questionId\": 144,\n        \"name\": \"fichier\",\n        \"answer\": false,\n        \"selected\": false\n      }\n    ]\n  }\n]'),
(10, 'c++', 'lanagage c ', 0x6c6f676f746573742e6a7067, '2020-05-06', '2020-05-26', 'technique', 4, 0, 0, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `testquestion`
--

DROP TABLE IF EXISTS `testquestion`;
CREATE TABLE IF NOT EXISTS `testquestion` (
  `id_test_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  PRIMARY KEY (`id_test_question`),
  KEY `id_test` (`id_test`),
  KEY `id_question` (`id_question`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `testquestion`
--

INSERT INTO `testquestion` (`id_test_question`, `id_test`, `id_question`) VALUES
(11, 2, 3),
(12, 2, 4),
(13, 2, 5),
(14, 7, 511),
(17, 7, 144),
(18, 10, 4281);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastpasswordresetdate` datetime NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `useradresse` varchar(255) NOT NULL,
  `userdate` datetime DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `usertel` varchar(255) NOT NULL,
  `disponibilite` bit(1) DEFAULT NULL,
  `poste` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `email`, `enabled`, `firstname`, `lastpasswordresetdate`, `lastname`, `password`, `useradresse`, `userdate`, `username`, `usertel`, `disponibilite`, `poste`, `id`) VALUES
(3, 'tes@gmail.com', b'1', 'test', '2020-05-27 11:54:28', 'test', 'TEST', 'sfax', '2020-05-23 18:00:53', 'test', '12345687', b'1', 'ChefDepartement', 187),
(4, 'khaled', b'1', 'khaled', '2020-05-27 11:54:20', 'rejeb', '$2a$10$nPPAaW/zyx9f3OBK3ZlSbe4C4eqzqVBH.hAxG6KGAM2yoYOePCX.G', 'tunis ', '2020-05-23 18:08:11', 'khaled', '25987412', b'1', 'Admin', 188),
(5, 'mohamed@medianet.com', b'1', 'mohamed', '2020-05-27 11:53:50', 'hossin', '$2a$10$I7yAUYCclTpeEY.9ppGj4eMaKty/Osdo3TqNYUfJTObva4pvgsMpu', 'sfax', '2020-05-25 13:41:18', 'mohamed', '9287412', b'1', 'Admin', 189);

-- --------------------------------------------------------

--
-- Structure de la table `user_authority`
--

DROP TABLE IF EXISTS `user_authority`;
CREATE TABLE IF NOT EXISTS `user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  KEY `FKgvxjs381k6f48d5d2yi11uh89` (`authority_id`),
  KEY `FKpqlsjpkybgos9w2svcri7j8xy` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_authority`
--

INSERT INTO `user_authority` (`user_id`, `authority_id`) VALUES
(187, 4),
(188, 5),
(189, 6);

-- --------------------------------------------------------

--
-- Structure de la table `user_seq`
--

DROP TABLE IF EXISTS `user_seq`;
CREATE TABLE IF NOT EXISTS `user_seq` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_seq`
--

INSERT INTO `user_seq` (`next_val`) VALUES
(5),
(5);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `candidat_reponse_choix`
--
ALTER TABLE `candidat_reponse_choix`
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_1` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id_candidat`),
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_2` FOREIGN KEY (`id_choix`) REFERENCES `choix` (`id_choix`),
  ADD CONSTRAINT `candidat_reponse_choix_ibfk_3` FOREIGN KEY (`id_reponse`) REFERENCES `reponse_candidat` (`id_reponse`);

--
-- Contraintes pour la table `choix`
--
ALTER TABLE `choix`
  ADD CONSTRAINT `choix_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id_candidat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `testquestion`
--
ALTER TABLE `testquestion`
  ADD CONSTRAINT `testquestion_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE,
  ADD CONSTRAINT `testquestion_ibfk_2` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user_authority`
--
ALTER TABLE `user_authority`
  ADD CONSTRAINT `FKgvxjs381k6f48d5d2yi11uh89` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
